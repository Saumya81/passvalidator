package password;

import static org.junit.Assert.*;

import org.junit.Test;

/*
 * @author Ramses Trejo , 023069
 * 
 * Assumption is that spaces are not considered valid characters
 */

public class PasswordValidatorTest {

	@Test
	public void testIsValidLengthRegular( ) {
		assertTrue( "Invalid password length" , PasswordValidator.isValidLength( "12345667890" ) );
	}
	
	@Test
	public void testIsValidLengthException( ) {
		assertFalse( "Invalid password length" , PasswordValidator.isValidLength( null ) );
	}	
	
	@Test
	public void testIsValidLengthExceptionSpaces( ) {
		assertFalse( "Invalid password length" , PasswordValidator.isValidLength( "          " ) );
	}		
	
	@Test
	public void testIsValidLengthBoundaryIn( ) {
		assertTrue( "Invalid password length" , PasswordValidator.isValidLength( "12345678" ) ); 
	}	
	
	@Test
	public void testIsValidLengthBoundaryOut( ) { 	
		assertFalse( "Invalid password length" , PasswordValidator.isValidLength( "1234567" ) ); 
	}		
	
}
